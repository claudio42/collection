import { useEffect } from "react";

const Card = ({ character, setFavorites, favorites, isRemovable = false }) => {
  const handleAddFavorite = (char) => {
    const alreadyExist = favorites.find((element) => element.id === char.id);
    if (alreadyExist) return;
    setFavorites([...favorites, char]);
  };

  const handleRemoveFavorite = (char) => {
    const newList = favorites.filter((character) => character.id !== char.id);
    localStorage.setItem("favoriteList", JSON.stringify(newList));
    setFavorites(newList);
  };

  useEffect(() => {
    localStorage.setItem("favoriteList", JSON.stringify(favorites));
  }, [favorites]);

  return (
    <>
      <span>{character.name} * </span>
      {isRemovable ? (
        <button onClick={() => handleRemoveFavorite(character)}>
          Remove Favorites
        </button>
      ) : (
        <button onClick={() => handleAddFavorite(character)}>
          Add to favorites
        </button>
      )}
    </>
  );
};

export default Card;
