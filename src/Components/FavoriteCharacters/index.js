import Card from "../Card";

const FavoriteCharacters = ({ favorites, setFavorites }) => {
  return (
    <>
      <h2>Favorite List</h2>
      {favorites.map((character, index) => {
        return (
          <Card
            key={index}
            character={character}
            favorites={favorites}
            setFavorites={setFavorites}
            isRemovable
          />
        );
      })}
    </>
  );
};

export default FavoriteCharacters;
