import Card from "../Card";

const Characters = ({ characters, setFavorites, favorites }) => {
  return (
    <>
      <h2>List</h2>
      {characters.map((character, index) => (
        <Card
          key={index}
          character={character}
          setFavorites={setFavorites}
          favorites={favorites}
        ></Card>
      ))}
    </>
  );
};

export default Characters;
