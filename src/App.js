import "./App.css";
import { Link, Route, Switch } from "react-router-dom";

import Characters from "./Components/Characters";
import FavoriteCharacters from "./Components/FavoriteCharacters";
import { useEffect, useState } from "react";
import axios from "axios";
function App() {
  const [characters, setCharacters] = useState([]);

  const getCharacters = () => {
    axios
      .get("https://rickandmortyapi.com/api/character")
      .then((res) => setCharacters(res.data.results))
      .catch((res) => console.log("error", res));
  };

  const [favorites, setFavorites] = useState(() => {
    const favoriteList = localStorage.getItem("favoriteList");
    if (favoriteList) {
      return JSON.parse(favoriteList);
    }
    return [];
  });

  useEffect(getCharacters, []);

  return (
    <div className="App">
      <div className="App-header">
        <div>
          <Link to={"/"}>Home</Link>
          <Link to={"/favorite"}>Favoritos</Link>
        </div>
        <Switch>
          <Route exact path="/">
            <Characters
              characters={characters}
              favorites={favorites}
              setFavorites={setFavorites}
            ></Characters>
          </Route>
          <Route path="/favorite">
            <FavoriteCharacters
              favorites={favorites}
              setFavorites={setFavorites}
            ></FavoriteCharacters>
          </Route>
        </Switch>
      </div>
    </div>
  );
}

export default App;
